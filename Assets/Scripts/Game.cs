using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Game : MonoBehaviour
{
    public static Game game;
    [SerializeField] private int startLivesCount = 3;
    [SerializeField] private TextMeshProUGUI livesText;
    [SerializeField] private TextMeshProUGUI CoinsText;
    private int livesCount;
    private int coinsCount;

    private void Awake()
    {
        if (game == null) 
        { 
            game = this;
            DontDestroyOnLoad(gameObject);
        } 
        else Destroy(gameObject);
   }

    private void Start()
    {
        livesCount = startLivesCount;
        coinsCount = 0;
        ShowLives();
        ShowCoins();
    }

    public void LoseLive()
    {
        livesCount--;
        ShowLives();
    }

    public void AdCoin(int amount)
    {
        coinsCount += amount;
        ShowCoins();
    }
    
    private void ShowLives()
    {
        livesText.text = livesCount.ToString();
    }

    private void ShowCoins()
    {
        CoinsText.text = coinsCount.ToString();
    }

}
