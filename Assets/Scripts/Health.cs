using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour
{
    //[SerializeField] private int lives = 3;
    private Vector3 startposition;

    private void Start()
    {
        startposition = transform.position;
    }

    
    public void TakeDamage()
    {
        Game.game.LoseLive();
        transform.position = startposition;
    }
}
